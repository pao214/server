<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Complaints extends REST_Controller{

	function __construct() {
		parent::__construct();
	}

	function individual_get(){
		$sess_user = $this->session->all_userdata();
		$this->load->model('Model_individual');
		if (isset($sess_user['user_info'])) {
			$user = $sess_user['user_info']['info'];
			$comps = $this->Model_individual->get_many_by(array('SenderID'=> $user['UserID']));
			$after_proc = [];
			foreach ($comps as $row) {
				array_push($after_proc, array('id'=> $row['id'], 'Title'=> $row['Title'], 'Content'=> $row['Content'],
					'CreatedTime'=> $row['CreatedTime'], 'UpdatedTime'=> $row['UpdatedTime'], 'Resolved'=> $row['Resolved']));
			}
			$this->response(array('success'=> true, 'Message'=> '', 'complaints'=>$after_proc));
		}else{
			$this->response(array('success'=> false, 'Message'=> 'User session expired.', 'complaints'=>[]));
		}
	}

	function work_get(){
		$sess_user = $this->session->all_userdata();
		$this->load->model('Model_individual');
		if (isset($sess_user['user_info'])) {
			$user = $sess_user['user_info']['info'];
			$comps = $this->Model_individual->get_many_by(array('ReceiverID'=> $user['UserID']));
			$after_proc = [];
			foreach ($comps as $row) {
				array_push($after_proc, array('id'=> $row['id'], 'Title'=> $row['Title'], 'Content'=> $row['Content'],
					'CreatedTime'=> $row['CreatedTime'], 'UpdatedTime'=> $row['UpdatedTime'], 'Resolved'=> $row['Resolved']));
			}
			$this->response(array('success'=> true, 'Message'=> '', 'complaints'=>$after_proc));
		}else{
			$this->response(array('success'=> false, 'Message'=> 'User session expired.', 'complaints'=>[]));
		}
	}

	function hostel_get(){
		$sess_user = $this->session->all_userdata();
		$this->load->model('Model_hostel');
		if (isset($sess_user['user_info'])) {
			$user = $sess_user['user_info']['info'];
			if (isset($user['HostelName'])) {
				$comps = $this->Model_hostel->get_many_by(array('HostelName'=> $user['HostelName']));
				$after_proc = [];
				foreach ($comps as $row) {
					array_push($after_proc, array('id'=> $row['id'], 'Title'=> $row['Title'], 'Content'=> $row['Content'],
						'CreatedTime'=> $row['CreatedTime'], 'UpdatedTime'=> $row['UpdatedTime'],
						'Upvotes'=>$row['Upvotes'], 'Downvotes'=> $row['Downvotes'], 'Resolved'=> $row['Resolved']));
				}
				$this->response(array('success'=> true, 'Message'=> '', 'complaints'=>$after_proc));
			}
		}else{
			$this->response(array('success'=> false, 'Message'=> 'User session expired.', 'complaints'=>[]));
		}
	}

	function institute_get(){
		$sess_user = $this->session->all_userdata();
		$this->load->model('Model_institute');
		if (isset($sess_user['user_info'])) {
			$user = $sess_user['user_info']['info'];
			$comps = $this->Model_institute->get_all();
			$after_proc = [];
			foreach ($comps as $row) {
				array_push($after_proc, array('id'=> $row['id'], 'Title'=> $row['Title'], 'Content'=> $row['Content'],
					'CreatedTime'=> $row['CreatedTime'], 'UpdatedTime'=> $row['UpdatedTime'],
					'Upvotes'=>$row['Upvotes'], 'Downvotes'=> $row['Downvotes'], 'Resolved'=> $row['Resolved']));
			}
			$this->response(array('success'=> true, 'Message'=> '', 'complaints'=>$after_proc));
		}else{
			$this->response(array('success'=> false, 'Message'=> 'User session expired.', 'complaints'=>[]));
		}
	}
}

?>